<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('register');
});
Auth::routes();

Route::get('/logout', [
    function () {
    return redirect('register');},
    'as' => 'logout'
]);

Route::get('/home', 'ApplicationController@index')->middleware('auth');
Route::get('/add-application',
     'ApplicationController@addPageApplication'
);
Route::post('/create-application', [
    'uses' => 'ApplicationController@createApplication',
    'as' => 'create.application'
]);
Route::get('/delete-application/{id}', [
    'uses' => 'ApplicationController@deleteApplication',
    'as' => 'delete.application'
]);
Route::get('/edit-application/{id}', [
    'uses' => 'ApplicationController@editPageApplication',
    'as' => 'edit.application'
    ]);
Route::post('/edit-application/{id}', [
    'uses' => 'ApplicationController@updateApplication',
    'as' => 'update.application'
]);
Route::post('/change-status', [
    'uses' => 'ApplicationController@changeStatusAjax',
     'as' => 'change.status']);

Route::get('/account/{id}', [
    'uses' => 'UserController@showAccount',
    'as' => 'account'
]);
Route::post('/account-update', [
    'uses' => 'UserController@saveAccount',
    'as' => 'account.save'
]);

Route::get('/write-file', 'ExportApplicationController@exportCsv');

Route::get('/write-excel', 'ExportApplicationController@exportExcel');

Route::get('/users', 'UserController@showUsers')->middleware('auth');

Route::get('/user-applications/{id}', [
    'uses' => 'ApplicationController@getUserApplications',
    'as' => 'user.applications'
]);

Route::get('/create-friend/{id}', [
    'uses' => 'FriendController@createUserFriend',
    'as' => 'create.friend'
]);
Route::get('/add-friend/{id}', [
    'uses' => 'FriendController@confirmUserFriend',
    'as' => 'add.friend'
]);

Route::get('/chat',['uses' => 'MessagesController@showMessages',
    'as' => 'user.messages'
]);

Route::get('/create-message/{id}', [
    'uses' => 'MessagesController@createMessage',
    'as' => 'create.message'
])->middleware('auth');
Route::post('/send-message/{id}', [
    'uses' => 'MessagesController@sendMessageToFriend',
    'as' => 'send.message'
]);
Route::get('/receive-message/{id}', [
    'uses' => 'MessagesController@receiveMessageFromFriend',
    'as' => 'receive.message'
]);
Route::get('/delete-message/{id}', [
    'uses' => 'MessagesController@deleteMessage',
    'as' => 'delete.message'
]);

Route::get('/message/{id}', [
    'uses' => 'MessagesController@showMessage',
    'as' => 'show.message'
]);

Route::get('/delete-user/{id}', [
    'uses' => 'UserController@deleteUser',
    'as' => 'delete.user'
]);
Route::get('/delete-friend/{id}', [
    'uses' => 'FriendController@deleteFriend',
    'as' => 'delete.friend'
]);

Route::get('/friends', 'FriendController@showFriends')->middleware('auth');
Route::get('/list', 'ApplicationController@showList')->middleware('auth');
Route::get('/sort-want', 'ApplicationController@wantSortApplications')->middleware('auth');
Route::get('/sort-need', 'ApplicationController@needSortApplications')->middleware('auth');