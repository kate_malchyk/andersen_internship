<?php

namespace App\Http\Controllers;
use App\Http\Requests\MessageRequest;
use App\Models\Message;
use App\User;
use Auth;


class MessagesController extends Controller
{
    /**
     * @param $friend_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function createMessage($friend_id)
    {
        $user = User::findById($friend_id);

        return view('create_message', ['user' => $user]);
    }

    /**
     * @param MessageRequest $request
     * @param $friend_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function sendMessageToFriend(MessageRequest $request, $friend_id)
    {
        $auth = Auth::user();
        $data = array(
            'from' => $auth->id,
            'to' => $friend_id,
            'content' => $request['content'],
            'status' => 1,
            'date' => date('Y-m-d'),
            'title' => $request['title'],
        );
        Message::create($data);

        return redirect()->back();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showMessages()
    {
        $auth = Auth::user(); //add message from

        return view('user_messages')
        ->with(array('listSendMessage' => $this->getListMessageFromUser($auth->id),
            'listReceiveMessages'=> $this->getListMessageFromFriend($auth->id)));
    }

    /**
     * @param $id
     * @return mixed
     */
    private function getListMessageFromUser($id)
    {
        $messageModel = new Message();
        $listSendMessage = $messageModel->getSendMessages($id);

        return $listSendMessage;
    }

    /**
     * @param $id
     * @return mixed
     */
    private function getListMessageFromFriend($id)
    {
        $messageModel = new Message();
        $listReceivedMessage = $messageModel->getReceiveMessages($id);

        return $listReceivedMessage;
    }

    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteMessage($id)
    {
        $message = Message::where('id', $id)->first();
        $message->delete();

        return redirect()->back();
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showMessage($id)
     {
        $message = Message::findById($id);
        $auth = Auth::user();
        if($message->status === 1 && $message->from != $auth->id)
        {
            $message->status = 2;
        }
        $message->save();

        return view('message',['message'=>$message]);

     }
}
