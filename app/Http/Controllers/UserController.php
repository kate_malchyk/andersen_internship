<?php

namespace App\Http\Controllers;
use App\Models\Application;
use App\Models\Avatar;
use App\Http\Requests\AccountRequest;
use App\Models\Friend;
use App\Models\Message;
use App\User;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showAccount()
    {
        $user = User::getUserJoinImage();

        return view('account', ['user'=> $user]);
    }

    /**
     * @return mixed
     */
    public function getListUsers()
    {
        $listUsers = User::getUserWithFriend();

        return $listUsers;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showUsers(){

        return view('users', ['listUsers'=> $this->getListUsers()]);
    }

    /**
     * @param AccountRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function saveAccount(AccountRequest $request)
    {
        $user = Auth::user();
        $user->name = $request['name'];
        $user->surname = $request['surname'];
        $user->age = $request['age'];
        $user->date = $request['date'];
        $user->country = $request['country'];
        $user->city = $request['city'];
        if ($request->hasFile('avatar'))
        {
            $avatar = $request->file('avatar');
            $fileName = $avatar->getClientOriginalName();
            $destinationPath = 'images/avatars/';
            $avatar->move($destinationPath, $fileName);
            $avatarModel = new Avatar();
            $avatarModel = $avatarModel->getAvatarsByUser($user->id);

            if (!$avatarModel)
            {
                $avatarModel = new Avatar();
                $avatarModel->user_id = $user->id;
            }
            $avatarModel->url = $fileName;
            $avatarModel->save();
        }
        $user->save();

        return redirect()->to(url('/account'));
    }
   /* public function deleteUser($id){
        //$auth = Auth::user();
        $user = User::where('id', $id)->first();
        $user->delete();

        $userApplications = Application::getApplicationsByUser($id);
        $userApplications->delete();

        $friends = Friend::getUserFriends($id);
        $friends->delete();


        $userMessages = Message::findByUser($id);
        $userMessages->delete();

        return redirect()->back();
    }*/



}
