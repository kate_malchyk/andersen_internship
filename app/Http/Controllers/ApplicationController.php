<?php

namespace App\Http\Controllers;

use App\Http\Requests\ApplicationRequest;
use App\Models\Application;
use App\Models\Friend;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ApplicationController extends Controller
{
    const perPage = 4;

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $auth = Auth::user();
        return view('home', ['listApplication' => $this->getListApplicationByUser($auth->id)]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showList()
    {
        $auth = Auth::user();
        $currentPage = isset($_GET['page']) ? (int)$_GET['page'] : 1;
        $countItems = Application::getApplicationsCountByUser($auth->id);
        $countPages = $countItems/self::perPage;

        if($countPages == 0)
        {
            $countPages =1;
        }

        if ($countItems%self::perPage != 0 )
        {
                $countPages = intval($countPages) + 1;
        }

        $pagesList = range(1,$countPages);
        $items = $this->getListPagination($auth->id, $currentPage, self::perPage);

        return view('list')->with(array('items' => $items, 'pagesList' => $pagesList, 'currentPage' => $currentPage, 'countPages' =>$countPages));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getUserApplications($id)
    {
        $auth = Auth::user();
        $userFriend = new Friend();
        $userFriend = $userFriend->getFriendByUserStatus($id, $auth->id);
        $userListApplication = $this->getListApplicationByUser($id);

        if ($userFriend)
        {
            $user = User::getUserInfo($id);
            //pagination
            $currentPage = isset($_GET['page']) ? (int)$_GET['page'] : 1;
            $countItems = Application::getApplicationsCountByUser($userFriend->friend_id);
            $countPages = $countItems/self::perPage;

            if($countPages == 0)
            {
                $countPages =1;
            }
            if ($countItems%self::perPage != 0 )
            {
                $countPages = intval($countPages) + 1;
            }
            $pagesList = range(1,$countPages);
            $items = $this->getListPagination($userFriend->friend_id, $currentPage, self::perPage);
            return view('users_applications')->with(array('userListApplication' => $items, 'user' => $user, 'pagesList' => $pagesList, 'currentPage' => $currentPage, 'countPages' => $countPages, 'id' =>$userFriend->friend_id));
        }
        $user = User::getUserInfo($id);

        return view('error_users')->with(array('userListApplication' => $userListApplication, 'user' => $user));
    }

    /**
     * @return Application[]|\Illuminate\Database\Eloquent\Collection
     */
    private function getListApplicationByUser($id)
    {
        $applicationModel = new Application();
        $listApplication = $applicationModel->getApplicationsByUserPaginate($id);

        return $listApplication;
    }
    /**
     * @param $id
     * @return Application[]|\Illuminate\Database\Eloquent\Collection
     */
    private function getSortByWantListApplication($id)
    {
        $applicationModel = new Application();
        $listApplication = $applicationModel->getWantSortApplications($id);

        return $listApplication;
    }
    /**
     * @param $id
     * @return mixed
     */
    private function getSortByNeedListApplication($id)
    {
        $applicationModel = new Application();
        $listApplication = $applicationModel->getNeedSortApplications($id);

        return $listApplication;
    }
    /**
     * @param $id
     * @return mixed
     */
    private function getListByUser($id)
    {
        $applicationModel = new Application();
        $listApplication = $applicationModel->getApplicationsByUser($id);

        return $listApplication;
    }
    /**
     * @param $id
     * @param $currentPage
     * @param $perPage
     * @return mixed
     */
    private function getListPagination($id, $currentPage, $perPage)
    {
        $applicationModel = new Application();
        $listApplication = $applicationModel->getItemsPagination($perPage, $currentPage, $id);

        return $listApplication;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function addPageApplication()
    {
        return view('create_application');
    }

    /**
     * @param ApplicationRequest $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createApplication(ApplicationRequest $request)
    {
        $auth = Auth::user();
        $application = new Application();
        $application->title = $request['title'];
        $application->want = $request['want'];
        $application->need = $request['need'];
        $application->price = $request['price'];
        $application->date = date('Y-m-d');
        $application->status = 0;
        $application->user_id = $auth->id;
        $application->save();

        return redirect()->to(url('/home'));
    }
    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function deleteApplication($id)
    {
        $application = Application::where('id', $id)->first();
        $application->delete();

        return redirect()->back();
    }
    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editPageApplication($id)
    {
        $applicationModel = new Application();
        $application = $applicationModel->findById($id);

        return view('edit_application', ['application' => $application]);
    }

    /**
     * @param ApplicationRequest $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function updateApplication(ApplicationRequest $request, $id)
    {
        $applicationModel = new Application();
        $application = $applicationModel->findById($id);
        $application->title = $request['title'];
        $application->want = $request['want'];
        $application->need = $request['need'];
        $application->price = $request['price'];
        $application->save();

        return redirect()->to(url('/home'));
    }

    /**
     * @param Request $request
     * @return false|string
     */
    public function changeStatusAjax(Request $request)
    {
        $applicationModel = new Application();
        $application = $applicationModel->findById($request['id']);
        $application->status = (int)$request['status'];
        if (!empty($application)) {
            try {
                $application->save();
            } catch (\Exception $exception) {

                return json_encode($exception->getMessage());
            }
        }
        return json_encode(true);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function wantSortApplications()
    {
        $auth = Auth::user();

        return view('home', ['listApplication' => $this->getSortByWantListApplication($auth->id)]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function needSortApplications()
    {
        $auth = Auth::user();

        return view('home', ['listApplication' => $this->getSortByNeedListApplication($auth->id)]);
    }
}
