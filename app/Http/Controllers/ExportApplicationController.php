<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;
use App\Export\ApplicationExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\Auth;
use App\Models\Application;

class ExportApplicationController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function exportExcel()
    {
        Excel::store(new ApplicationExport(), 'excelApplications.xlsx');

        return Excel::download(new ApplicationExport(), 'excelApplications.xlsx');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function exportCsv()
    {
        Storage::put('file.csv', $this->getListApplication());

        return Storage::download('file.csv');
    }

    /**
     * @return mixed
     */
    private function getListApplication()
    {
        $auth = Auth::user();
        $applicationModel = new Application();
        $listApplication = $applicationModel->getApplicationsByUser($auth->id);

        return $listApplication;
    }
}
