<?php

namespace App\Http\Controllers;

use App\Http\NotificationsServices\EmailMessageService;
use Illuminate\Support\Facades\Auth;
use App\Models\Friend;
use App\User;


class FriendController extends Controller
{

    /**
     * @param int $friend_id
     * @param $auth
     * @return Friend
     */
    public function ifFriend(int $friend_id, $auth)
    {
        $friend = new Friend();
        $friend = $friend->getFriendRequest($friend_id, $auth->id);

        return $friend;
    }

    /**
     * @param int $friend_id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function createUserFriend(int $friend_id) //нажатие на кнопку Add
    {
        $auth = Auth::user();
        $newFriend = $this->ifFriend($friend_id, $auth);

        if (!$newFriend)
        {
            $newFriend = new Friend();
            $newFriend->user_id = $auth->id;
            $newFriend->friend_id = $friend_id;
            $newFriend->status = '2';

            $userFriend = new Friend();
            $userFriend->user_id = $friend_id;
            $userFriend->friend_id = $auth->id;
            $userFriend->status = '4';

            $newFriend->save();
            $userFriend->save();

            $emailMessage = new EmailMessageService();
            $emailMessage->send();
        } else
            {
            if ($newFriend)
            {
                $newFriend->status = '3';
                $newFriend->update();
            }
            $userFriend = Friend::getFriendByUser($friend_id, $auth->id);
            if ($userFriend)
            {
                $userFriend->status = '3';
                $userFriend->update();
            }
            }

        return redirect()->back();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showFriends()
    {
        return view('friends', ['listFriends' => $this->getListFriends()]);
    }

    /**
     * @return mixed
     */
    public function getListFriends()
    {
        $listFriends = User::getUserWithFriend()->where('status', '=', 3);
        return $listFriends;
    }
    /*  public function confirmUserFriend(int $friend_id) //резултьтат после нажатия на confirm
        {
             $auth = Auth::user();
             $friendOfUser = $this->ifFriend($friend_id, $auth);
             if($friendOfUser) {
                 $friendOfUser->status = '3';
             }
                 $userFriend = new Friend();
                 $userFriend = $userFriend->getFriendByUser($friend_id,$auth->id);
                 if($userFriend)
                 {
                     $userFriend->status = '3';
             }
             $friendOfUser->update();
             $userFriend->update();

         return redirect()->back();
        }*/
}
