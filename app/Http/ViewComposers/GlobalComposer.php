<?php
/**
 * Created by PhpStorm.
 * User: kate_pc
 * Date: 08.10.18
 * Time: 17:21
 */

namespace App\Http\ViewComposers;
use Illuminate\View\View;
use Illuminate\Support\Facades\Auth;
use App\Models\Message;

class GlobalComposer {

    public function compose(View $view)
    {
        $auth  = \Auth::user();
        if($auth)
        {
            $view->with('count', Message::where('to','=',$auth->id )
            ->where('status','=',1)
            ->count());
        }
    }
}