<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AccountRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //return false;
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'string|max:255|min:3',
            'surname' => 'string|max:255',
            'age' =>'numeric',
            'date' => 'date:Y-m-d',
            'country' =>'string|max:255',
            'city' =>'string|max:255',
        ];
    }
}
