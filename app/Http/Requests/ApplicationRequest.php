<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ApplicationRequest extends FormRequest
{
    public function authorize()
    {
        //return false;
        return true;
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return
        [
            'title' => 'required|string|max:255|min:3',
            'want' =>'|required|numeric|min:1|between:0,10',
            'need' =>'|required|numeric|min:1|between:0,10',
            'price' =>'required|numeric',
        ];
    }
}


