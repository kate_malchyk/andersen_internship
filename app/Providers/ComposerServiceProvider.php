<?php
/**
 * Created by PhpStorm.
 * User: kate_pc
 * Date: 08.10.18
 * Time: 17:20
 */

namespace App\Providers;


use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\View\Factory as ViewFactory;

class ComposerServiceProvider extends ServiceProvider {

    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot(ViewFactory $view)
    {
        $view->composer('*', 'App\Http\ViewComposers\GlobalComposer');
    }

    public function register()
    {
        //
    }

}