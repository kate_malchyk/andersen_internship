<?php
/**
 * Created by PhpStorm.
 * User: kate_pc
 * Date: 21.09.18
 * Time: 15:49
 */

namespace App\Export;
use App\Models\Application;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ApplicationExport implements FromCollection, WithHeadings, ShouldAutoSize
{
    public function collection()
    {
        $auth = Auth::user();
        $applicationModel = new Application();
        $listApplication = $applicationModel->getApplicationsByUser($auth->id);
        return $listApplication;
    }
    public function headings(): array
    {
        return [
            '#',
            'User id',
            'Title',
            'Want',
            'Need',
            'Price',
            'Created at',
            'Updated at',
            'Status',
            'Date',
        ];
    }

}