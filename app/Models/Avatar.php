<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Avatar extends Model
{
    protected $fillable = [
        'url',
    ];

    /**
     * @param $user_id
     * @return mixed
     */
    public function getAvatarsByUser($user_id)
    {
        $result = Avatar::where('user_id','=',$user_id)->first();

        return $result;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function findById($id)
    {
        $result = Avatar::where('id','=',$id)
            ->first();

        return $result;
    }
}
