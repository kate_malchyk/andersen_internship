<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = [
        'from',
        'to',
        'content',
        'status',
        'date',
        'title',
    ];

    /**
     * @param $id
     * @return mixed
     */
    public static function getSendMessages($id)
    {
        $result = Message::selectRaw('messages.*, users.name')
            ->leftjoin('users', 'messages.to', '=', 'users.id')
            ->where('messages.from','=',$id)
            ->get();

        return $result;
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function getReceiveMessages($id)
    {
        $result = Message::selectRaw('messages.*, users.name')
            ->leftjoin('users', 'messages.from', '=', 'users.id')
            ->where('messages.to','=',$id)
            ->get();

        return $result;
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function findById($id)
    {
        $result = Message::where('id','=',$id)
            ->first();

        return $result;
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function findByUser($id)
    {
        $result = Message::where('to','=',$id)
            ->where('from','=',$id)
            ->first();

        return $result;
    }
/*
    public function getMessagesByUser($user_id)
    {
        $result = Message::where('user_id','=',$user_id)->get();
        return $result;
    }*/
}
