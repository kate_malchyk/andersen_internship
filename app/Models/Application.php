<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    protected $fillable = [
        'title',
        'want',
        'need',
        'price',
        'date',
        'status',
        ];
    /**
     * @return Application[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getWantSortApplications($user_id)
    {
        $result = Application::where('user_id','=',$user_id)
            ->orderBy('want', 'desc')
            ->paginate(4);

        return $result;
    }

    /**
     * @param $user_id
     * @return mixed
     */
    public function getNeedSortApplications($user_id)
    {
        $result = Application::where('user_id','=',$user_id)
            ->orderBy('need', 'desc')
            ->paginate(4);

        return $result;
    }

    /**
     * @param $user_id
     * @return mixed
     */
    public function getApplicationsByUserPaginate($user_id)
    {
        $result = Application::where('user_id','=',$user_id)
            ->paginate(4);

        return $result;
    }

    /**
     * @param $user_id
     * @return mixed
     */
    public static function getApplicationsByUser($user_id)
    {
        $result = Application::where('user_id','=',$user_id)
            ->first();

        return $result;
    }

    /**
     * @param $user_id
     * @return mixed
     */
    public static function getApplicationsCountByUser($user_id)
    {
        $result = Application::where('user_id','=',$user_id)
            ->count();

        return $result;
    }
    /**
     *
     * @param $id
     * @return mixed
     */
    public function findById($id)
    {
        $result = Application::where('id','=',$id)
            ->first();

        return $result;
    }

    /**
     * @param $perPage
     * @param $currentPage
     * @param $user_id
     * @return mixed
     */
    public static function getItemsPagination($perPage, $currentPage, $user_id)
    {
        $result = Application::where('user_id','=',$user_id)
            ->offset($perPage * ($currentPage - 1))
            ->limit($perPage)
            ->get();

        return $result;
    }
}
