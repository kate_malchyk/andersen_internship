<?php
/**
 * Created by PhpStorm.
 * User: kate_pc
 * Date: 27.09.18
 * Time: 12:59
 */
namespace App\Models;
use Illuminate\Database\Eloquent\Model;

class Friend extends Model
{
    protected $fillable = [
        'user_id',
        'friend_id',
        'status'
    ];

    /**
     * @param $friend_id
     * @param $user_id
     * @return mixed
     */
    public static function getFriendByUserStatus($friend_id, $user_id)
    {
        $result = Friend::where('friend_id','=',$friend_id)
            ->where('user_id','=',$user_id)
            ->where('status','=',3)
            ->first();

        return $result;
    }

    /**
     * @param $friend_id
     * @param $user_id
     * @return mixed
     */
    public static function getFriendByUser($friend_id, $user_id)
    {
        $result = Friend::where('friend_id','=',$friend_id)
            ->where('user_id','=',$user_id)
            ->first();

        return $result;
    }

    /**
     * @param $friend_id
     * @param $user_id
     * @return mixed
     */
    public function getFriendRequest($friend_id, $user_id)
    {
        $result = Friend::where('friend_id','=',$user_id)
            ->where('user_id','=',$friend_id)
            ->first();

        return $result;
    }

    /**
     * @param $user_id
     * @return mixed
     */
    public static function getUserFriends($user_id)
    {
        $result = Friend::where('friend_id','=',$user_id)
            ->where('user_id','=',$user_id)
            ->first();

        return $result;
    }



}