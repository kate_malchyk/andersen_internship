<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'surname', 'age', 'date' => 'date:Y.m.d', 'country', 'city', 'email', 'password',
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return mixed
     */
    public static function getUserJoinImage()
    {
        $auth = \Auth::user();
        $result = User::selectRaw('users.*, avatars.url')
            ->where('users.id', '=', $auth->id)
            ->leftjoin('avatars', 'users.id', '=', 'avatars.user_id')
            ->first();

        return $result;
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function getUserJoinImageById($id)
    {
        $result = User::selectRaw('users.*, avatars.url')
            ->where('users.id', '=', $id)
            ->leftjoin('avatars', 'users.id', '=', 'avatars.user_id')
            ->first();

        return $result;
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function getUserInfo($id)
    {
        $result = User::selectRaw('users.*, avatars.url')
            ->where('users.id', '=', $id)
            ->leftjoin('avatars', 'users.id', '=', 'avatars.user_id')
            ->first();

        return $result;
    }

    /**
     * @return mixed
     */
    public static function getUserWithFriend()
    {
        $auth = \Auth::user();
        $result = User::selectRaw('users.*, friends.status')
            ->leftJoin('friends', function($join) use ($auth)
            {
                $join->on('friends.friend_id', '=', 'users.id')
                    ->where('friends.user_id', '=', $auth->id);
            })
            ->where('users.id', '!=', $auth->id)
            ->get();

        return $result;
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function findById($id)
    {
        $result = User::where('id','=',$id)
            ->first();

        return $result;
    }
  /*  public static function getUserNoFriendByID($id)
    {
        $auth = \Auth::user();
        $result = User::selectRaw('users.*')
            ->leftjoin('friends', function($join) use ($auth)
            {
                $join->on('friends.friend_id', '=', 'users.id')
                    ->where('friends.friend_id', '!=', $auth->id);
            })
            ->where('users.id', '=', $id)
            ->get();

        return $result;
    }*/

}