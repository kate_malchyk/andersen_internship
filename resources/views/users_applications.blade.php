@extends('layouts.app')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <h2 style="margin-bottom: 20px;">{{ $user->name }}'s Profile</h2>
                <div>
                    <img src="/images/avatars/{{$user->getUserInfo($user->id)->url ? $user->getUserInfo($user->id)->url : "default.jpg"}}" style="width:200px; height:200px; float:left; margin-bottom:20px; margin-left:80px;border-radius:50%; margin-right:25px;" class="leftimg">
                <p style = "text-align: center;">First Name : {{ $user->name }}</p>
                <p style = "text-align: center">Last Name: {{ $user->surname }}</p>
                <p style = "text-align: center">Age: {{ $user->age }}</p>
                <p style = "text-align: center">Date of Birth {{ $user->date }}</p>
                <p style = "text-align: center">Country: {{ $user->country }}</p>
                <p style = "text-align: center">City: {{ $user->city }}</p>
                </div>
                <div style = "margin-left: 120px;">
                <button id = 'message_status' onclick="window.location = '{{route('create.message', ['id' => $user->id])}}'" class="btn btn-primary">
                    {{ __('Send message') }}
                </button>
                </div>
                <table>
                <tr>
                    <th>
                        Title
                    </th>
                    <th>
                        Want
                    </th>
                    <th>
                        Need
                    </th>
                    <th>
                        Price
                    </th>

                    <th>
                        Created
                    </th>
                    <th>
                        Updated
                    </th>
                    <th>
                        Status
                    </th>
                </tr>
         @foreach($userListApplication as $application)
                <tr>
                <td>
                    {{$application->title}}
                </td>
                <td>
                    {{$application->want}}
                </td>
                <td>
                    {{$application->need}}
                </td>
                <td>
                    {{$application->price}}
                </td>
                <td>
                    {{$application->created_at}}
                </td>
                <td>
                    {{$application->updated_at}}
                </td>
                <td>
                    <input  type="checkbox"  name="status" class="status" id="{{$application->id}}" {{!empty($application->status) ? 'checked' : ''}}>
                </td>
            </tr>
        @endforeach

                    <ul class = "pagination">
                        @if($currentPage != 1)
                            <li>
                                <a href = "/user-applications/{{$id}}?page={{($currentPage==1) ? $currentPage : $currentPage-1 }}">Назад <<</a>
                            </li>
                        @endif
                        @foreach($pagesList as $pageNumber)

                            <li>
                                <a href = "/user-applications/{{$id}}?page={{$pageNumber}}">{{$pageNumber}}</a>
                            </li>

                        @endforeach
                        @if($currentPage != $countPages)
                            <li>
                                <a href = "/user-applications/{{$id}}?page={{($currentPage == $countPages) ? $currentPage : $currentPage+1 }}">>>Вперед </a>
                            </li>
                        @endif
                    </ul>
</div>
</div>
    </p>
</div>
@endsection
