@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <section class = "row new-post">
                    <div class ="col-md-6 col-md-offset-10">

                        <header>
                            <h5>Create your wish!</h5>
                        </header>
                        <form action="{{route('create.application')}}" method="post">

                            <div class = "form-group ">
                                <label for = "title">Title:</label>
                                <input class="form-control" type="text" name="title" id ="title">
                            </div>

                            <div class = "form-group ">
                                <label for = "want">Want:</label>
                                <input class="form-control" type="number" name="want" id ="want">
                            </div>

                            <div class = "form-group ">
                                <label for = "need">Need:</label>
                                <input class="form-control" type="number" name="need" id ="need">
                            </div>

                            <div class = "form-group ">
                                <label for = "price">Set a price:</label>
                                <input class="form-control" type="number" name="price" id ="price">
                            </div>
                            <button type ="submit" class ="btn btn-primary">Create an application </button>
                            <input type="hidden" value="{{Session::token()}}" name="_token">
                        </form>
                    </div>
                </section>
                <div class="card-body">

                </div>
            </div>
        </div>
    </div>

</div>
@endsection