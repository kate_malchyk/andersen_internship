


<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Wishes</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->

    </head>
    <body>
                <div class="title m-b-md">
                    <button id='fff' onclick="window.location='http://www.wishes.loc/register'" >Register</button>
                </div>
                <div class="content">

                </div>

                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script>

            $('#fff').click(function(){
                $.ajax(
                    {url: '{{url("get_ajax")}}',
                    success:
                        function(result){
                            $('.content').text(result);
                    }});

            });

    </script>
    </body>
</html>
