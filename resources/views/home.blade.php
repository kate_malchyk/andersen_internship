@extends('layouts.app')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

@section('content')



<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    Sorting <span class="caret"></span>
                </a>
                <div class="dropdown-menu dropdown-menu" >
                    <div>
                        <a href = "{{url('/sort-want')}}">Sort by want</a>
                    </div>
                    <div>
                        <a href = "{{url('/sort-need')}}">Sort by need</a>
                    </div>
                </div>
                </li>
                <table>
                <tr>
                    <th>
                        Title
                    </th>
                    <th>
                        Want
                    </th>
                    <th>
                        Need
                    </th>
                    <th>
                        Price
                    </th>

                    <th>
                        Created
                    </th>
                    <th>
                        Updated
                    </th>
                    <th>
                        Status
                    </th>
                    <th colspan="2">
                        Action
                    </th>
                </tr>
         @foreach($listApplication as $application)
                <tr>
                <td>
                    {{$application->title}}
                </td>
                <td>
                    {{$application->want}}
                </td>
                <td>
                    {{$application->need}}
                </td>
                <td>
                    {{$application->price}}
                </td>
                <td>
                    {{$application->created_at}}
                </td>
                <td>
                    {{$application->updated_at}}
                </td>
                <td>
                    <input  type="checkbox"  name="status" class="status" id="{{$application->id}}" {{!empty($application->status) ? 'checked' : ''}}>
                </td>
                <td>
                    <a href= "{{ route('delete.application', ['id' => $application->id]) }}">Delete</a>
                </td>
                <td>
                    <a href= "{{ route('edit.application', ['id' => $application->id]) }}"> Edit
                    </a>
                </td>
            </tr>
        @endforeach
                </table>
               <?php echo  $listApplication->render(); ?>
                <div>
                    Create a new application:
                </div>
                <div class="form-group row mb-0">
                    <div> <button onclick="window.location = '{{url('add-application')}}'" class="btn btn-primary" style = "margin-left: 20px;">
                            {{ __('Create') }}
                        </button></div>
                <div class="card-body">
                </div>
                    <p>Write to the file:</p>
                    <div class="form-group row mb-3">
                        <div class="col-md-8 offset-md-4">
                            <button onclick="window.location = '{{url('write-file')}}'" class="btn btn-primary">
                                {{ __('Write') }}
                            </button>
                            <button onclick="window.location = '{{url('write-excel')}}'" class="btn btn-primary" style ="margin-top:10px;margin-right: 30px;">
                                {{ __('Write in excel') }}
                            </button>
                        </div>
            </div>
        </div>
    </div>
</div>

<script>
        $(document).ready(function () {
            $('.status').change(function () {
                var status = 0;
                if ($(this).prop("checked")) {
                    status = 1;
                } else {
                    status = 0;
                }
                $.ajax(
                    {
                        method: "POST",
                        url: '/change-status',
                        data: { _token: '{{Session::token()}}', id: $(this).attr("id"), status: status }
                    });
            });
        });
    </script>
@endsection
