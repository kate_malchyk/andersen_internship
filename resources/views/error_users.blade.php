@extends('layouts.app')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <p>
                    "Error!!! User <a href= "{{ route('user.applications', ['id' => $user->id])}}">{{$user->name}}</a> not a friend"
                </p>

            </div>

            <div style = "margin-left: 120px;">
                <button id = 'message_status' onclick="window.location = '{{route('create.message', ['id' => $user->id])}}'" class="btn btn-primary">
                    {{ __('Send message') }}
                </button>
            </div>
    </div>
</div>
@endsection
