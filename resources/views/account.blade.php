@extends('layouts.app')

@section('title')
Profile page
@endsection

@section('content')

    <div class="row justify-content-center">

        <a href= "{{ route('delete.user', ['id' => $user->id]) }}">Delete</a>

    <form action="{{ route('account.save') }}" method="post" enctype="multipart/form-data">
         <h2 style="margin-bottom: 10px;">{{ $user->name }}'s Profile</h2>
            <img src="/images/avatars/{{$user->getUserJoinImage()->url ? $user->getUserJoinImage()->url : "default.jpg"}}" style="width:200px; height:200px; float:left; margin-bottom: 20px; border-radius:50%; margin-right:25px;" >
        <input type="file" name="avatar">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class ="col-md-6">
                <div class="form-group">
                    <label for="name">First Name</label>
                    <input type="text" name="name" class="form-control" value="{{ $user->name }}" id="name">
                </div>
                <div class="form-group">
                    <label for="surname">Last Name</label>
                    <input type="text" name="surname" class="form-control" value="{{ $user->surname }}" id="surname">
                </div>
                <div class="form-group">
                    <label for="age">Age</label>
                    <input type="text" name="age" class="form-control" value="{{ $user->age }}" id="age">
                </div>

                <div class="form-group">
                    <label for="date">Date of Birth</label>
                    <input type="text" name="date" class="form-control" value="{{ $user->date }}" id="date">
                </div>
                <div class="form-group">
                    <label for="country">Country</label>
                    <input type="text" name="country" class="form-control" value="{{ $user->country }}" id="country">
                </div>
                <div class="form-group">
                    <label for="city">City</label>
                    <input type="text" name="city" class="form-control" value="{{ $user->city }}" id="city">
                </div>
                <button type="submit" class="btn btn-primary">Save Account</button>
                <input type="hidden" value="{{ Session::token() }}" name="_token">
        </div>
        </form>
    </div>

@endsection
