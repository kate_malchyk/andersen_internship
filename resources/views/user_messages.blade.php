@extends('layouts.app')

@section('title')
    Chat
@endsection

@section('content')
    <h2 style = "text-align: center">{{Auth::user()->name}}'s chat</h2>

    <p>Outgoing messages :</p>
<table style="text-align: center">
    <tr>
        <th>
            Title
        </th>
        <th>
            Send at
        </th>
        <th>
            To
        </th>
        <th>
            Action
        </th>
    </tr>
    @foreach($listSendMessage as $message)
        <tr>
            <td>
                <a href= "{{ route('show.message', ['id' => $message->id]) }}">{{$message->title}}</a>
            </td>
            <td>
                {{$message->created_at}}
            </td>
            <td>
                <img src="/images/avatars/{{ \App\User::getUserJoinImageById($message->to)->url ? \App\User::getUserJoinImageById( $message->to)->url : "default.jpg" }}" style="width:32px; height:32px; top:5px; left:100px; border-radius:50%;"> <a href= "{{ route('user.applications', ['id' => $message->to])}}">{{$message->name}}</a>
            </td>
            <td>
                <a href= "{{ route('delete.message', ['id' => $message->id]) }}">Delete</a>
            </td>
        </tr>
    @endforeach
</table>
<div style ="margin-top: 30px; margin-bottom: 20px;">Incoming messages:</div>
<table style ="text-align:center;">
    <tr>
        <th>
            Title
        </th>
        <th>
            Send at
        </th>
        <th>
            From
        </th>
        <th colspan="2">
            Action
        </th>
    </tr>
    @foreach($listReceiveMessages as $message)
        <tr>
            <td>
                <a href= "{{ route('show.message', ['id' => $message->id]) }}">{{$message->title}}</a>
            </td>
            <td>
                {{$message->created_at}}
            </td>
            <td>
                <img src="/images/avatars/{{ \App\User::getUserJoinImageById($message->from)->url ? \App\User::getUserJoinImageById( $message->from)->url : "default.jpg" }}" style="width:32px; height:32px; top:5px; left:100px; border-radius:50%;"> <a href= "{{ route('user.applications', ['id' => $message->from])}}">{{$message->name}}</a>
            </td>
            <td>
                <a href= "{{ route('delete.message', ['id' => $message->id]) }}">Delete</a>
            </td>
            <td>
                <button id = 'message_status' onclick="window.location = '{{route('create.message', ['id' => $message->from])}}'" class="btn btn-primary">
                    {{ __('Reply') }}
                </button>
            </td>

        </tr>
    @endforeach
</table>
@endsection
