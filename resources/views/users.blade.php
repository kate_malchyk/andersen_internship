@extends('layouts.app')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <table>
                <tr>
                    <th>
                       First Name
                    </th>
                    <th>
                        Second Name
                    </th>
                    <th colspan="2">
                        Action
                    </th>
                </tr>
         @foreach($listUsers as $user)
                <tr>
                <td>
                    <img src="/images/avatars/{{ \App\User::getUserJoinImageById($user->id)->url ? \App\User::getUserJoinImageById( $user->id)->url : "default.jpg" }}" style="width:32px; height:32px; top:5px; left:100px; border-radius:50%;"> {{$user->name}}
                </td>
                <td>
                    {{$user->surname}}
                </td>
                <td>
                    @if ($user->status == 3)
                        <a href= "{{ route('user.applications', ['id' => $user->id])}}">Show</a>
                    @elseif ($user->status == null)
                        <button id = 'status' onclick="window.location = '{{route('create.friend', ['id' => $user->id])}}'" class="btn btn-primary">
                            {{ __('Add Friend') }}
                        </button>
                    @elseif ($user->status == 2)
                        <p>
                            Request was send. Waiting for confirmation...Message was send
                        </p>
                    @elseif($user->status == 4)
                          <button  onclick="window.location = '{{route('create.friend', ['id' => $user->id])}}'" class="btn btn-primary">
                            {{ __('Confirm a request') }}
                        </button>
                    @endif
                </td>
                    <td>
                        <button id = 'message_status' onclick="window.location = '{{route('create.message', ['id' => $user->id])}}'" class="btn btn-primary">
                            {{ __('Send message') }}
                        </button>
                       {{-- @if ($userMessage->status == 3)
                            <p> Message was send</p>
                        @endif--}}
                        {{--<button id = 'message_status' onclick="window.location = '{{route('read.message', ['id' => $user->id])}}'" class="btn btn-primary">
                            {{ __('Read') }}
                        </button>--}}
                    </td>
            </tr>
        @endforeach
                </table>

            </div>
    </div>
</div>
@endsection
