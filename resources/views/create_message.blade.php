@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

<section class = "row new-post">
<div class ="col-md-6 col-md-offset-10">
    <header>
        <h5>Create a message to friend</h5>
    </header>
            <form action="{{route('send.message', ['id' => $user->id] )}}" method="post">

                <div class = "form-group ">
                    <label for = "title">Title:</label>
                    <input class="form-control" type="text" name="title" id ="title">
                </div>

                <div class = "form-group ">
                    <label for = "content">Content of the letter:</label>
                    <input class="form-control" type="text" name="content" id ="content">
                </div>
                <button type ="submit" class ="btn btn-primary">Submit</button>
                <input type="hidden" value="{{Session::token()}}" name="_token">
            </form>
        </div>
</section>
</div>
</div>
</div>
</div>
@endsection