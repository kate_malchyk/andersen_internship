@extends('layouts.app')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <table>
                <tr>
                    <th>
                       First Name
                    </th>
                    <th colspan="2">
                        Action
                    </th>
                </tr>
         @foreach($listFriends as $user)
                <tr>
                <td>
                    <img src="/images/avatars/{{ \App\User::getUserJoinImageById($user->id)->url ? \App\User::getUserJoinImageById( $user->id)->url : "default.jpg" }}" style="width:32px; height:32px; top:5px; left:100px; border-radius:50%;" >
                    <a href= "{{ route('user.applications', ['id' => $user->id])}}">{{$user->name}}</a>
                </td>
                    <td>
                    <button id = 'message_status' onclick="window.location = '{{route('create.message', ['id' => $user->id])}}'" class="btn btn-primary">
                        {{ __('Send message') }}
                    </button>
                    </td>
                    <td>
                        <a href= "{{ route('delete.friend', ['id' => $user->id]) }}">Delete</a>
                    </td>
                </tr>
        @endforeach
                </table>

            </div>
    </div>
</div>
@endsection
