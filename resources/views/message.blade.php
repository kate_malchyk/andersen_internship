@extends('layouts.app')
@section('content')
<div class="container">
<div class="row justify-content-center">
<div class="col-md-8">
<div class="card">

<section class = "row new-post">
<div class ="col-md-6 col-md-offset-10">
    <header>
        <h5>My message from </h5>
    </header>
<form action="{{route('show.message', ['id' => $message->id] )}}" method="post">

<div class = "form-group ">
<label for = "title">Title:</label>
    <input class="form-control" type="text" name="title" id ="title" value="{{ $message->title }}">
     </div>
<div class = "form-group ">
<label for = "content">Content of the letter:</label>
    <input class="form-control" type="text" name="content" id ="content" value="{{ $message->content }}" >
    </div>
        </form>
        </div>
</section>
    <div><button id = 'message_status' onclick="window.location = '{{route('create.message', ['id' => $message->from])}}'" class="btn btn-primary">
            {{ __('Reply') }}
        </button></div>
</div>
</div>
</div>
</div>
@endsection