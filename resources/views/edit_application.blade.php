@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <section class = "row new-post">
                    <div class ="col-md-6 col-md-offset-10">

                        <header>
                            <h5>Edit your wish!</h5>
                        </header>
                        <form action= "{{route('update.application', ['id' => $application->id])}}" method="post" >
                            <div class = "form-group ">
                                <label for = "title">Update Title:</label>
                                <input class="form-control" type="text" name="title" id ="title" value= "{{$application->title}}" >
                            </div>

                            <div class = "form-group ">
                                <label for = "want">Update Want:</label>
                                <input class="form-control" type="number" name="want" id ="want" value="{{$application->want}}">
                            </div>

                            <div class = "form-group ">
                                <label for = "need">Update Need:</label>
                                <input class="form-control" type="number" name="need" id ="need" value="{{$application->need}}" >
                            </div>

                            <div class = "form-group ">
                                <label for = "price">Update a price:</label>
                                <input class="form-control" type="number" name="price" id ="price" value="{{$application->price}}">
                            </div>

                            <button type ="submit" class ="btn btn-primary">Update an application </button>
                            <input type="hidden" value="{{Session::token()}}" name="_token">
                        </form>
                    </div>
                </section>
                <div class="card-body">

                </div>
            </div>
        </div>
    </div>

</div>
@endsection