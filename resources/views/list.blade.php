@extends('layouts.app')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">

                <table>
                <tr>
                    <th>
                        Title
                    </th>
                    <th>
                        Want
                    </th>
                    <th>
                        Need
                    </th>
                    <th>
                        Price
                    </th>

                    <th>
                        Created
                    </th>
                    <th>
                        Updated
                    </th>
                    <th>
                        Status
                    </th>
                    <th colspan="2">
                        Action
                    </th>
                </tr>
         @foreach($items as $application)
                        <tr>
                <td>
                    {{$application->title}}
                </td>
                <td>
                    {{$application->want}}
                </td>
                <td>
                    {{$application->need}}
                </td>
                <td>
                    {{$application->price}}
                </td>
                <td>
                    {{$application->created_at}}
                </td>
                <td>
                    {{$application->updated_at}}
                </td>
                <td>
                    <input  type="checkbox"  name="status" class="status" id="{{$application->id}}" {{!empty($application->status) ? 'checked' : ''}}>
                </td>
                <td>
                    <a href= "{{ route('delete.application', ['id' => $application->id]) }}">Delete</a>
                </td>
                <td>
                    <a href= "{{ route('edit.application', ['id' => $application->id]) }}"> Edit
                    </a>
                </td>
            </tr>
        @endforeach
                </table>
        </div>
            <ul class = "pagination">
                @if($currentPage != 1)
                <li>
                    <a href = "/list?page={{($currentPage==1) ? $currentPage : $currentPage-1 }}">Назад <<</a>
                </li>
                @endif
                @foreach($pagesList as $pageNumber)

                    <li>
                        <a href = "/list?page={{$pageNumber}}">{{$pageNumber}}</a>
                    </li>

                @endforeach
                    @if($currentPage != $countPages)
                <li>
                    <a href = "/list?page={{($currentPage == $countPages) ? $currentPage : $currentPage+1 }}">>>Вперед </a>
                </li>
                        @endif
            </ul>
    </div>
</div>

<script>
        $(document).ready(function () {
            $('.status').change(function () {
                var status = 0;
                if ($(this).prop("checked")) {
                    status = 1;
                } else {
                    status = 0;
                }
                $.ajax(
                    {
                        method: "POST",
                        url: '/change-status',
                        data: { _token: '{{Session::token()}}', id: $(this).attr("id"), status: status }
                    });
            });
        });
    </script>
@endsection
